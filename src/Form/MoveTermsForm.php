<?php

namespace Drupal\taxonomy_manager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\taxonomy\VocabularyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for deleting given terms.
 */
class MoveTermsForm extends FormBase {

  use MessengerTrait;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * MoveTermsForm constructor.
   *
   * @param \Drupal\taxonomy\TermStorageInterface $termStorage
   *   The taxonomy term storage.
   */
  public function __construct(TermStorageInterface $termStorage) {
    // Object with methods to manage terms e.g. \Drupal\taxonomy\TermStorage
    $this->termStorage = $termStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('taxonomy_term')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @TODO: Add autocomplete to select/add parent term.
   */
  public function buildForm(array $form, FormStateInterface $form_state, VocabularyInterface $taxonomy_vocabulary = NULL, $selected_terms = []) {
    if (empty($selected_terms)) {
      $form['info'] = [
        '#markup' => $this->t('Please select the terms you want to move.'),
      ];
      return $form;
    }

    // Cache form state so that we keep the parents in the modal dialog.
    $form_state->setCached(TRUE);
    $form['voc'] = ['#type' => 'value', '#value' => $taxonomy_vocabulary];
    $form['selected_terms']['#tree'] = TRUE;

    $items = [];
    foreach ($this->termStorage->loadMultiple($selected_terms) as $term) {
      $items[] = $term->label();
      $form['selected_terms'][$term->id()] = ['#type' => 'value', '#value' => $term->id()];
    }

    $form['terms'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => $this->t('Selected terms to move:'),
    ];

    $form['target_voc'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Vocabulary to move the term to'),
      '#target_type' => 'taxonomy_vocabulary',
      '#description' => $this->t('Enter a vocabulary you want the selected term(s) to move to. Keep blank to select current vocabulary'),
      '#maxlength' => 1024,
      '#tags' => TRUE,
    ];

    $form['parents'] = array(
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Parent term(s)'),
      '#target_type' => $this->termStorage->getEntityType()->id(),
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => [$taxonomy_vocabulary->id()]
      ],
      '#description' => $this->t('Enter a term name. Separate multiple parent terms with commas. Leave empty to move terms to the root of vocabulary.'),
      '#maxlength' => 1024,
      '#tags' => TRUE,
    );


    $form['keep_old_parents'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep old parents and add new ones (multi-parent). Otherwise old parents get replaced.'),
    ];

    $form['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Move'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $parents = $form_state->getValue('parents');
    if (is_array($parents)) {
      $parents = array_map(function($item) {
        return $item['target_id'];
      }, $parents);

      // Make sure no terms becomes parent of itself.
      if (count(array_intersect($form_state->getValue('selected_terms'), $parents)) > 0) {
        $form_state->setError($form['parents'], $this->t('You are requesting to make a term parent of itself.'));
      }

      // Make sure no circular hierarchy would be introduced with this change.
      $all_parents = [];
      foreach ($parents as $parent) {
        foreach ($this->termStorage->loadAllParents($parent) as $all_parent) {
          $all_parents[] = $all_parent->id();
        }
      }
      if (count(array_intersect($form_state->getValue('selected_terms'), $all_parents)) > 0) {
        $form_state->setError($form['parents'], $this->t('The resulting hierarchy would contain circles, which is not allowed. A term cannot be a parent of itself.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /**
     * @var VocabularyInterface
     */
    $taxonomy_vocabulary = $form_state->getValue('voc');

    $selected_terms = $this->termStorage->loadMultiple($form_state->getValue('selected_terms'));
    $keep_old_parents = $form_state->getValue('keep_old_parents');
    $parents = $form_state->getValue('parents');
    $target_vocabulary = $form_state->getValue('target_voc');
    if (!is_array($parents)) {
      $parents = [['target_id' => 0]];
    }

    $is_multi_parents = FALSE;

    foreach ($selected_terms as $term) {
      if ($keep_old_parents) {
        foreach ($this->termStorage->loadParents($term->id()) as $existing_parent) {
          $term->parent[] = $existing_parent->id();
        }
        if (empty($term->parent)) {
          $term->parent[] = 0;
        }
      }
      foreach ($parents as $parent) {
        $term->parent[] = $parent['target_id'];
      }

      if (count($term->parent) > 1) {
        $is_multi_parents = TRUE;
      }

      if ($target_vocabulary) {
        $term->set('vid', $target_vocabulary[0]['target_id']);
      }

      $term->save();
    }

    if ($is_multi_parents) {
      $taxonomy_vocabulary->setHierarchy(TAXONOMY_HIERARCHY_MULTIPLE)
        ->save();
    }

    $form_state->setRedirect('taxonomy_manager.admin_vocabulary', array('taxonomy_vocabulary' => $taxonomy_vocabulary->id()));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_manager_move_form';
  }

}
